var express = require('express');
var favicon = require('serve-favicon');
var path = require('path');
var app = express();

app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, '/views', '/img/favicon.ico')))

app.get('/', function(req, res) {
    res.render('./main.ejs', { page: "capetien"});
});

app.get('/auguste', function (req, res) {
    res.render('./main.ejs', {page: "auguste"});
});

app.get('/louix', function (req, res) {
    res.render('./main.ejs', { page: "louix" });
});

app.get('/lebel', function (req, res) {
    res.render('./main.ejs', { page: "lebel" });
});

app.get('/hugues', function (req, res) {
    res.render('./main.ejs', { page: "hugues" });
});

app.get('/adhesion', function (req, res) {
    res.render('./main.ejs', { page: "adhesion" });
});

app.use(express.static(__dirname + '/views'));

app.listen(8050);