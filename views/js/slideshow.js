// Création du slideshow automatique

var i = 0;
var images = ["img/philippe-auguste-01.jpg", "img/philippe-auguste-02.jpg"];
var time = 10000;

function changeImg() {
  document.slide.src = images[i];
  if (i < images.length - 1) {
    i++;
  } else {
    i = 0;
  }
  setTimeout("changeImg()", time);
}
window.onload = changeImg;
