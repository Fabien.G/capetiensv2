var i = 0;
var images = [
  "img/les-capetiens-et-le-pouvoir-d-etat.jpg",
  "img/hugues-capet-02.jpg",
  "img/louis-ix-02.jpg",
  "img/philippe-auguste-02.jpg",
  "img/philippe-le-bel-01.jpg",
  "img/philippe-le-bel-02.jpg"
];
var time = 10000;

function changeImg() {
  document.slide.src = images[i];
  if (i < images.length - 1) {
    i++;
  } else {
    i = 0;
  }
  setTimeout("changeImg()", time);
}
window.onload = changeImg;

var slider = document.getElementById("box");
var image = [
  "img/hugues-capet-02", 
  "img/louis-ix-02", 
  "img/philippe-auguste-02", 
  "img/philippe-le-bel-01", 
  "img/philippe-le-bel-02", 
  "img/les-capetiens-et-le-pouvoir-d-etat"
];

var b = image.length;

function nextImg() {
  if (b < image.length) {
    b = b + 1;
  } else {
    b = 1;
  }
  slider.innerHTML = "<img src=" + image[b - 1] + ".jpg>";
  changeImg();
}

function prevImg() {
  if (b < image.length + 1 && b > 1) {
    b = b - 1;
  } else {
    b = image.length;
  }
  slider.innerHTML = "<img src=" + image[b - 1] + ".jpg>";
  changeImg();
}
