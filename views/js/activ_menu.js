// Création du lien actif

function activeMenu(element) {
  // On récupère l'élément qui possède la classe active. Et si il existe on lui enlève
  let activeMenuItem = document.getElementsByClassName("active")[0];
  if (activeMenuItem !== undefined) {
    activeMenuItem.classList.toggle("active");
  }
  // On ajoute la classe active à l'élément sélectionné
  element.classList.add("active");
}
