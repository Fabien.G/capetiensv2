function schedule() {
  var timepiece = new Date();
  var clock = document.getElementById("clock");
  clock.innerHTML = timepiece.toLocaleTimeString();
  clock.setAttribute("style", "color: #5f5b5b;");
  if (timepiece.getMinutes() % 2 === 0) {
    clock.setAttribute("style", "color: white;");
  }
}
setInterval(schedule, 1000);
